class MicropostsController < ApplicationController
  before_action :set_micropost, only: [:show, :edit, :update, :destroy]

  # GET /microposts
  # GET /microposts.json
  def index
    @microposts = Micropost.all
    
    # Parse the URI and retrieve it to a temporary file
		news_tmp_file = open('http://tympanus.net/codrops/all-articles/page/1/')

		# Parse the contents of the temporary file as HTML
		doc = Nokogiri::HTML(news_tmp_file)

		# Define the css selectors to be used for extractions, most
		article_css_class         =".ct-archive-container"
		article_header_css_class  ="article.ct-box > h3 > a"
		article_image_css_class   ="//img[@class='attachment-thumbnail wp-post-image']/@src"
		article_summary_css_class =".ct-feat-excerpt"

		# extract all the articles 
		articles = doc.css(article_css_class)

		#html output
		title   = ""
		image   = ""
		summary = ""

		#extract the title from the articles
		articles.each do |article|
		  title    = article.search(article_header_css_class).map {|el| el.text}
		  image    = article.search(article_image_css_class).map {|a| a.value }
		  summary  = article.search(article_summary_css_class).map {|el| el.text}
		end
		
		# render :text => html,:content_type => "text/plain"
		@titles   = title
		@images   = image
		@summaries= summary
		
		@size = @titles.size
  end

  # GET /microposts/1
  # GET /microposts/1.json
  def show
  end

  # GET /microposts/new
  def new
    @micropost = Micropost.new
  end

  # GET /microposts/1/edit
  def edit
  end

  # POST /microposts
  # POST /microposts.json
  def create
    @micropost = Micropost.new(micropost_params)

    respond_to do |format|
      if @micropost.save
        format.html { redirect_to @micropost, notice: 'Micropost was successfully created.' }
        format.json { render :show, status: :created, location: @micropost }
      else
        format.html { render :new }
        format.json { render json: @micropost.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /microposts/1
  # PATCH/PUT /microposts/1.json
  def update
    respond_to do |format|
      if @micropost.update(micropost_params)
        format.html { redirect_to @micropost, notice: 'Micropost was successfully updated.' }
        format.json { render :show, status: :ok, location: @micropost }
      else
        format.html { render :edit }
        format.json { render json: @micropost.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /microposts/1
  # DELETE /microposts/1.json
  def destroy
    @micropost.destroy
    respond_to do |format|
      format.html { redirect_to microposts_url, notice: 'Micropost was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_micropost
      @micropost = Micropost.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def micropost_params
      params.require(:micropost).permit(:content, :user_id)
    end
end
