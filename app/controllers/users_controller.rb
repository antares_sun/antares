require 'open-uri'

class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  # GET /users
  # GET /users.json
  def index
    @users = User.all
    
    # Parse the URI and retrieve it to a temporary file
		news_tmp_file = open('https://news.google.com')

		# Parse the contents of the temporary file as HTML
		doc = Nokogiri::HTML(news_tmp_file)

		# Define the css selectors to be used for extractions, most
		article_css_class         =".esc-layout-article-cell"
		article_header_css_class  ="span.titletext"
		article_summary_css_class =".esc-lead-snippet-wrapper"
		
		# article_image_css_class ="img.esc-thumbnail-image"

		# extract all the articles 
		articles = doc.css(article_css_class)
		
		img = ""
		
		# images = doc
		
		# images.each do |image|
		#   main_image = image
		# end

		#html output
		html = ""
		
		main_image = ""

		#extract the title from the articles
		articles.each do |article|
		  title_nodes = article.css(article_header_css_class)
		  
		  main_image = article.search('//img[@class="esc-thumbnail-image"]/@src').map {|a| img = a.value }

		  # since there are multiple titles for each entry on google news
		  # for this demo we only want the first (topmost)
		  #
		  # its very easy to do, since title_nodes is of type NodeSet which implements Enumerable (http://ruby-doc.org/core-2.0.0/Enumerable.html)
		  # > title_nodes.class
		  #  => Nokogiri::XML::NodeSet 
		  # > title_nodes.class.ancestors
		  #   => [Nokogiri::XML::NodeSet, Enumerable, Object, Kernel, BasicObject]

		  prime_title = title_nodes.first


		  # Even when the css selector returns only one element, its type is also Nokogiri::XML::NodeSet
		  summary_node = article.css(article_summary_css_class) 
		  # > summary_node.class
		  #  => Nokogiri::XML::NodeSet 
		  # > summary_node.size
		  #  => 1 

      # Create an "---------" line for the title
      separator = "-" * prime_title.text.size
  
		  # Extracting the text from an Nokogiri::XML::Element is easy by calling the #text method, 
		  # notice how we can also do it on the NodeSet, 
		  # there it as a different semantic by invoking #text in all the children nodes
  	  html += "%s\n%s\n%s\n\n\n" % [prime_title.text, separator, summary_node.text]
		end
		
		@news = html
		
		@images = main_image
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:name, :email)
    end
end
