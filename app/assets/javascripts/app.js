$(document).ready(function(){

    $(window).scroll(function() {
    	var $child = $(".child-menu");
		if (($("header").length > 0)) { 
			if(($(this).scrollTop() > 250) && ($(window).width() > 767)) {
				// $("header").addClass("fixed-header-on");
				$($child).css({
				    'transform' : 'translateY(-100%)'
				});
				
				$("header").hover(
				    function(){
				        $($child).css({
        				    'transform' : 'translateY(0%)'
        				});
				    },
				    function(){
				        $($child).css({
        				    'transform' : 'translateY(-100%)'
        				});
				    }
				);
				
				$("footer").css({
				    'transform' : 'translateY(0%)'
				});
			} else {
				// $("header").removeClass("fixed-header-on");
				$($child).css({
				    'transform' : 'translateY(0%)'
				});
				
				$("header").hover(
				    function(){
				        $($child).css({
        				    'transform' : 'translateY(0%)'
        				});
				    },
				    function(){
				        $($child).css({
        				    'transform' : 'translateY(0%)'
        				});
				    }
				);
				
				$("footer").css({
				    'transform' : 'translateY(100%)'
				});
			}
		};
	});
})