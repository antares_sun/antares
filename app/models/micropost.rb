class Micropost < ActiveRecord::Base
    validates :content, length: { 
        maximum: 140,
        minimum: 5,
        too_short: "Minimal %{count} karakter",
        too_long: "Maksimal %{count} karakter"
    }
end
